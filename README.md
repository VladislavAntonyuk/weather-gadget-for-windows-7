# README #

Weather gadget for Windows 7.

### What is this repository for? ###

Display current weather on Windows sidebar.

### How do I get set up? ###

To install the gadget run Weather.gadget
To modify the gadget make your changes in Source folder, then create Zip archive with extension *.gadget.

### Contribution guidelines ###

You are free to help me with localization