////////////////////////////////////////////////////////////////////////////////
//
//  THIS CODE IS NOT APPROVED FOR USE IN/ON ANY OTHER UI ELEMENT OR PRODUCT COMPONENT.
//  Copyright (c) 2006 Microsoft Corporation.  All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////
var L_localizedStrings_Text = [];

////////////////////////////////////////////////////////////////////////////////
//
// Default values for this locale
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['DefaultCity'] = 'Redmond, WA';
L_localizedStrings_Text['DefaultLocationCode'] = 'wc:USWA0367';
L_localizedStrings_Text['DefaultUnit'] = 'Fahrenheit';
L_localizedStrings_Text['DefaultDockedSize'] = 'Small';
L_localizedStrings_Text['DefaultUnDockedSize'] = 'Tall';

////////////////////////////////////////////////////////////////////////////////
//
// GADGET
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['Culture'] = 'en-US';
L_localizedStrings_Text['ServiceNotAvailable'] = 'Service not available';
L_localizedStrings_Text['GettingData'] = 'Getting data...';
L_localizedStrings_Text['LocationDontExist'] = 'Location not found';
L_localizedStrings_Text['ServiceNotAvailableInYourArea'] = 'Service not available in your language or region.';
L_localizedStrings_Text['Searching'] = 'Searching...';
L_localizedStrings_Text['NoSearchQuery'] = 'No search query specified.';
L_localizedStrings_Text['NoResults'] = 'No results returned.';
L_localizedStrings_Text['Night-New'] = 'New Moon';
L_localizedStrings_Text['Night-Waxing-Crescent'] = 'Waxing Crescent Moon';
L_localizedStrings_Text['Night-First-Quarter'] = 'First Quarter Moon';
L_localizedStrings_Text['Night-Waxing-Gibbous'] = 'Waxing Gibbous Moon';
L_localizedStrings_Text['Night-Full'] = 'Full Moon';
L_localizedStrings_Text['Night-Waning-Gibbous'] = 'Waning Gibbous Moon';
L_localizedStrings_Text['Night-Last-Quarter'] = 'Last Quarter Moon';
L_localizedStrings_Text['Night-Waning-Crescent'] = 'Waning Crescent Moon';
L_localizedStrings_Text['CurrentSky'] = 'Details...';
L_localizedStrings_Text['Current/Forcasts'] = 'Current/Forcast: ';
L_localizedStrings_Text['Feelslike'] = 'Feels Like: ';
L_localizedStrings_Text['Humidity'] = 'Humidity: ';
L_localizedStrings_Text['WindSpeed'] = 'Wind Speed: ';
L_localizedStrings_Text['PrecipitationChance'] = 'Precipitation Chance: ';
L_localizedStrings_Text['ObservPoint'] = 'Observation Point: ';
L_localizedStrings_Text['ObservTime'] = 'Observation Time: ';
L_localizedStrings_Text['SunriseAt'] = 'Sunrise ';
L_localizedStrings_Text['NoonAt'] = 'Noon ';
L_localizedStrings_Text['SunsetAt'] = 'Sunset ';
L_localizedStrings_Text['Close'] = 'Close';

////////////////////////////////////////////////////////////////////////////////
//
// SETTINGS
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['DisplayTemperatureIn'] = 'Unit of measure: ';
L_localizedStrings_Text['Fahrenheit'] = '&#176;F, mph';
L_localizedStrings_Text['Celsius'] = '&#176;C, km/hr';
L_localizedStrings_Text['Search'] = 'Search';
L_localizedStrings_Text['CurrentCity'] = 'Current location: ';
L_localizedStrings_Text['EnterACityName'] = 'Search for location';
L_localizedStrings_Text['SearchNearbyDisambiguation'] = 'closest location for ';
L_localizedStrings_Text['SearchFuzzyDisambiguation'] = 'closest location: ';
L_localizedStrings_Text['Tall'] = 'Big';
L_localizedStrings_Text['Small'] = 'Small';
L_localizedStrings_Text['DockedSize'] = 'Size on Sidebar: ';
L_localizedStrings_Text['UnDockedSize'] = 'Size on Desktop: ';