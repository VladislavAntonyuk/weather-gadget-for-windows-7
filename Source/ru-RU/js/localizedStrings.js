////////////////////////////////////////////////////////////////////////////////
//
//  THIS CODE IS NOT APPROVED FOR USE IN/ON ANY OTHER UI ELEMENT OR PRODUCT COMPONENT.
//  Copyright (c) 2006 Microsoft Corporation.  All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////
var L_localizedStrings_Text = [];

////////////////////////////////////////////////////////////////////////////////
//
// Default values for this locale
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['DefaultCity'] = 'Днепр, Украина';
L_localizedStrings_Text['DefaultLocationCode'] = 'wc:UPXX0010';
L_localizedStrings_Text['DefaultUnit'] = 'Celsius';
L_localizedStrings_Text['DefaultDockedSize'] = 'Small';
L_localizedStrings_Text['DefaultUnDockedSize'] = 'Tall';

////////////////////////////////////////////////////////////////////////////////
//
// GADGET
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['Culture'] = 'ru-RU';
L_localizedStrings_Text['ServiceNotAvailable'] = 'Сервис не доступен';
L_localizedStrings_Text['GettingData'] = 'Получение данных...';
L_localizedStrings_Text['LocationDontExist'] = 'Местоположение не найдено.';
L_localizedStrings_Text['ServiceNotAvailableInYourArea'] = 'Сервис не доступен в вашем регионе.';
L_localizedStrings_Text['Searching'] = 'Поиск...';
L_localizedStrings_Text['NoSearchQuery'] = 'Поисковый запрос не указан.';
L_localizedStrings_Text['NoResults'] = 'Нет результатов поиска.';
L_localizedStrings_Text['Night-New'] = 'Новолуние';
L_localizedStrings_Text['Night-Waxing-Crescent'] = 'Прибывающая луна в первой четверти';
L_localizedStrings_Text['Night-First-Quarter'] = 'Луна в первой четверти';
L_localizedStrings_Text['Night-Waxing-Gibbous'] = 'Прибывающая луна между второй четвертью и полнолунием';
L_localizedStrings_Text['Night-Full'] = 'Полнолуние';
L_localizedStrings_Text['Night-Waning-Gibbous'] = 'Убывающая луна между второй четвертью и полнолунием';
L_localizedStrings_Text['Night-Last-Quarter'] = 'Луна в последней четверти';
L_localizedStrings_Text['Night-Waning-Crescent'] = 'Убывающая луна в последней четверти';
L_localizedStrings_Text['CurrentSky'] = 'Подробнее...';
L_localizedStrings_Text['Current/Forcast'] = 'Текущий прогноз: ';
L_localizedStrings_Text['Feelslike'] = 'Чувствуется как: ';
L_localizedStrings_Text['Humidity'] = 'Влажность: ';
L_localizedStrings_Text['WindSpeed'] = 'Скорость ветра: ';
L_localizedStrings_Text['PrecipitationChance'] = 'Вероятность осадков: ';
L_localizedStrings_Text['ObservPoint'] = 'Пункт наблюдения: ';
L_localizedStrings_Text['ObservTime'] = 'Время наблюдения: ';
L_localizedStrings_Text['SunriseAt'] = 'Восход ';
L_localizedStrings_Text['NoonAt'] = 'Полдень ';
L_localizedStrings_Text['SunsetAt'] = 'Заход ';
L_localizedStrings_Text['Close'] = 'Закрыть';

////////////////////////////////////////////////////////////////////////////////
//
// SETTINGS
//
////////////////////////////////////////////////////////////////////////////////
L_localizedStrings_Text['DisplayTemperatureIn'] = 'Показывать температуру в:';
L_localizedStrings_Text['Fahrenheit'] = 'градусах Фаренгейта';
L_localizedStrings_Text['Celsius'] = 'градусах Цельсия';
L_localizedStrings_Text['Search'] = 'Поиск';
L_localizedStrings_Text['CurrentCity'] = 'Текущее местоположение:';
L_localizedStrings_Text['EnterACityName'] = 'Поиск местоположения';
L_localizedStrings_Text['SearchNearbyDisambiguation'] = 'ближайшее местоположение для ';
L_localizedStrings_Text['SearchFuzzyDisambiguation'] = 'ближайшее местоположение:';
L_localizedStrings_Text['Tall'] = 'Большой';
L_localizedStrings_Text['Small'] = 'Маленький';
L_localizedStrings_Text['DockedSize'] = 'Размер панели виджетов: ';
L_localizedStrings_Text['UnDockedSize'] = 'Размер экрана: ';